[TOC]

**解决方案介绍**
===============
TongWeb应用服务器提供了各种容器和功能组件，包括Web容器、EJB容器、RMI服务容器、Web服务平台、JCA服务、数据库连接池、事务控制组件等，并支持各种成熟开发框架，以帮助企业快速构建各种业务应用处理系统，为企业级信息化建设构建基础应用平台。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-tongweb-app-server.html

**架构图**
---------------
![架构图](./document/tongweb.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建弹性云服务器 ECS，用于安装部署TongWeb环境。

2、创建弹性公网IP EIP，用于提供访问公网和被公网访问能力。

3、创建安全组，通过配置安全组访问规则保证网络安全。

**组织结构**
---------------

``` lua
rapid-deployment-of-tongweb-app-server
├── rapid-deployment-of-tongweb-app-server.tf.json -- 资源编排模板
```
**开始使用**
---------------
1. 修改初始化密码。登录[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，参考[在控制台重置弹性云服务器密码](https://support.huaweicloud.com/usermanual-ecs/zh-cn_topic_0067909751.html)，进行密码重置。

   **图1** 重置密码
   ![重置密码](./document/readme-image-001.png)

2. 登录[弹性云服务器 ECS](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，单击远程登录，或者使用其他的远程登录工具进入Linux弹性云服务器。

   **图2** 登录云服务器控制平台
   ![登录ECS云服务器控制平台](./document/readme-image-002.png)

   **图3** 登录Linux弹性云服务器
   ![登录Linux弹性云服务器](./document/readme-image-003.png)

3. 在Linux弹性云服务中输入账号和密码后回车。

   **图4** 登录弹性云服务器 ECS
   ![登录ECS弹性云服务器](./document/readme-image-004.png)

4. 在${TW7_HOME}/bin 目录下,通过 startserver.sh 启动应用服务器。也可以通过 startservernohup.sh 以后台运行的方式启动应用服务器。

   进入目录cd TongWeb7.0/bin/

   启动服务 sh startserver.sh

   **图5** 启动服务
   ![启动服务](./document/readme-image-005.png)

5. 访问服务，打开浏览器，输入http://EIP:9060/console , 访问页面如下图所示,默认登录用户名密码为：thanos/thanos123.com。可参考东方通应用商店中-TongWeb7用户使用手册-镜像版进行详细使用。

   **图6** 访问服务
   ![访问服务](./document/readme-image-006.png)